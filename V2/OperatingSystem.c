      #include "OperatingSystem.h"
#include "OperatingSystemBase.h"
#include "MMU.h"
#include "Processor.h"
#include "Buses.h"
#include "Heap.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

// Functions prototypes
void OperatingSystem_PrepareDaemons();
void OperatingSystem_PCBInitialization(int, int, int, int, int);
void OperatingSystem_MoveToTheREADYState(int);
void OperatingSystem_Dispatch(int);
void OperatingSystem_RestoreContext(int);
void OperatingSystem_SaveContext(int);
void OperatingSystem_TerminateProcess();
int OperatingSystem_LongTermScheduler();
void OperatingSystem_PreemptRunningProcess();
int OperatingSystem_CreateProcess(int);
int OperatingSystem_ObtainMainMemory(int, int);
int OperatingSystem_ShortTermScheduler();
int OperatingSystem_ExtractFromReadyToRun();
void OperatingSystem_HandleException();
void OperatingSystem_HandleSystemCall();
void OperatingSystem_PrintReadyToRunQueue();
void OperatingSystem_HandleClockInterrupt();
// The process table
PCB processTable[PROCESSTABLEMAXSIZE];

// Address base for OS code in this version
int OS_address_base = PROCESSTABLEMAXSIZE * MAINMEMORYSECTIONSIZE;

// Identifier of the current executing process
int executingProcessID=NOPROCESS;

// Identifier of the System Idle Process
int sipID;

// Begin indes for daemons in programList
int baseDaemonsInProgramList; 

// Array that contains the identifiers of the READY processes
int readyToRunQueue[NUMBEROFQUEUES][PROCESSTABLEMAXSIZE];
int numberOfReadyToRunProcesses[NUMBEROFQUEUES] = {0,0};

//Counter for knowing how many clock interrupts has happend
int numberOfClockInterrupts = 0;


 
// Variable containing the number of not terminated user processes
int numberOfNotTerminatedUserProcesses=0;

// Variables containing the queue of sleepingProcesses and 
// a counter of the sleeping processes
int sleepingProcessesQueue[PROCESSTABLEMAXSIZE];
int numberOfSleepingProcesses=0;

//List with state names
char * statesNames[5]={"NEW","READY","EXECUTING","BLOCKED","EXIT"}; 

// Initial set of tasks of the OS
void OperatingSystem_Initialize(int daemonsIndex) {
	
	int i, selectedProcess;
	FILE *programFile; // For load Operating System Code

	// Obtain the memory requirements of the program
	int processSize=OperatingSystem_ObtainProgramSize(&programFile, "OperatingSystemCode");

	// Load Operating System Code
	OperatingSystem_LoadProgram(programFile, OS_address_base, processSize);
	
	// Process table initialization (all entries are free)
	for (i=0; i<PROCESSTABLEMAXSIZE;i++)
		processTable[i].busy=0;
	
	// Initialization of the interrupt vector table of the processor
	Processor_InitializeInterruptVectorTable(OS_address_base+1);
		
	// Create all system daemon processes
	OperatingSystem_PrepareDaemons(daemonsIndex);
	
	// Create all user processes from the information given in the command line
	if(OperatingSystem_LongTermScheduler()<= 1)
	{
		OperatingSystem_ReadyToShutdown();
	}

	
	if (strcmp(programList[processTable[sipID].programListIndex]-> executableName,"SystemIdleProcess")) {
		// Show message "ERROR: Missing SIP program!\n"
		ComputerSystem_DebugMessage(21,SHUTDOWN);
		exit(1);		
	}

	// At least, one user process has been created
	// Select the first process that is going to use the processor
	
	selectedProcess=OperatingSystem_ShortTermScheduler();
	

	// Assign the processor to the selected process
	OperatingSystem_Dispatch(selectedProcess);

	// Initial operation for Operating System
	Processor_SetPC(OS_address_base);
}

// Daemon processes are system processes, that is, they work together with the OS.
// The System Idle Process uses the CPU whenever a user process is able to use it
void OperatingSystem_PrepareDaemons(int programListDaemonsBase) {
  
	// Include a entry for SystemIdleProcess at 0 position
	programList[0]=(PROGRAMS_DATA *) malloc(sizeof(PROGRAMS_DATA));

	programList[0]->executableName="SystemIdleProcess";
	programList[0]->arrivalTime=0;
	programList[0]->type=DAEMONPROGRAM; // daemon program

	sipID=INITIALPID%PROCESSTABLEMAXSIZE; // first PID for sipID

	// Prepare aditionals daemons here
	// index for aditionals daemons program in programList
	baseDaemonsInProgramList=programListDaemonsBase;

}



	
void OperatingSystem_HandleClockInterrupt(){
	int i;
	int pid;
	numberOfClockInterrupts++;
	int sleepingQueueModified = 0;
	
	OperatingSystem_ShowTime(ALL);
	ComputerSystem_DebugMessage(117,ALL, numberOfClockInterrupts);
	for( i = 0; i<numberOfSleepingProcesses; i++)
	{
		pid = sleepingProcessesQueue[i];
		if(processTable[pid].whenToWakeUp == numberOfClockInterrupts)
		{
			Heap_poll(sleepingProcessesQueue,QUEUE_WAKEUP, &numberOfSleepingProcesses);
			OperatingSystem_MoveToTheREADYState(pid);
			sleepingQueueModified = 1;
			i--;
		}

	}

	

	if( sleepingQueueModified)
	{
		int temPID = executingProcessID;
		int needsToInsertTheFirstElememtAgain = 1;
		pid =OperatingSystem_ShortTermScheduler();
		
		if(processTable[temPID].queueID ==DAEMONSQUEUE && processTable[pid].queueID ==USERPROCESSQUEUE )
		{
			OperatingSystem_PreemptRunningProcess();
			OperatingSystem_ShowTime(SHORTTERMSCHEDULE);
			ComputerSystem_DebugMessage(121, SHORTTERMSCHEDULE,temPID,pid );
			OperatingSystem_Dispatch(pid);
			needsToInsertTheFirstElememtAgain =0;
		}
		else if(processTable[temPID].queueID == processTable[pid].queueID )
		{
			if(processTable[temPID].priority > processTable[pid].priority)
			{
				OperatingSystem_PreemptRunningProcess();
				OperatingSystem_ShowTime(SHORTTERMSCHEDULE);
				ComputerSystem_DebugMessage(121, SHORTTERMSCHEDULE,temPID,pid );
				OperatingSystem_Dispatch(pid);
				needsToInsertTheFirstElememtAgain =0;
			}
		}

		if(needsToInsertTheFirstElememtAgain)
		{
			Heap_add(pid, readyToRunQueue[processTable[pid].queueID],QUEUE_PRIORITY ,&numberOfReadyToRunProcesses[processTable[pid].queueID] ,PROCESSTABLEMAXSIZE);
			
		}


		sleepingQueueModified = 0;
	
	}
	OperatingSystem_PrintStatus();

	} 
// The LTS is responsible of the admission of new processes in the system.
// Initially, it creates a process from each program specified in the 
// 			command lineand daemons programs
int OperatingSystem_LongTermScheduler() {
  
	int PID, i,
		numberOfSuccessfullyCreatedProcesses=0;
	
	for (i=0; programList[i]!=NULL && i<PROGRAMSMAXNUMBER ; i++) {
		PID=OperatingSystem_CreateProcess(i);
		
		if(PID  == NOFREEENTRY)
		{
			OperatingSystem_ShowTime(ERROR);
			ComputerSystem_DebugMessage(103,ERROR, programList[i]-> executableName);
			
			
		}
		else if( PID == PROGRAMDOESNOTEXIST)
		{
			OperatingSystem_ShowTime(ERROR);
			ComputerSystem_DebugMessage(104,ERROR, programList[i]-> executableName ,"---it does not exist---");
			
		}
		else if( PID == PROGRAMNOTVALID)
		{
			OperatingSystem_ShowTime(ERROR);
			ComputerSystem_DebugMessage(104,ERROR, programList[i]-> executableName ,"---invalid priority or size ---");
			
		}
		else if (PID == TOOBIGPROCESS)
		{
			OperatingSystem_ShowTime(ERROR);
			ComputerSystem_DebugMessage(105,ERROR, programList[i]-> executableName ,"---invalid priority or size ---");
			
		}
		else
		{
			numberOfSuccessfullyCreatedProcesses++;
			if (programList[i]->type==USERPROGRAM) 
				numberOfNotTerminatedUserProcesses++;
			// Move process to the ready state
			OperatingSystem_MoveToTheREADYState(PID);
		}

		
	}

		if(numberOfSuccessfullyCreatedProcesses > 0)
		{
			OperatingSystem_PrintStatus();
		}

	// Return the number of succesfully created processes
	return numberOfSuccessfullyCreatedProcesses;
}


// This function creates a process from an executable program
int OperatingSystem_CreateProcess(int indexOfExecutableProgram) {
  
	int PID;
	int processSize;
	int loadingPhysicalAddress;
	int priority;
	FILE *programFile;
	PROGRAMS_DATA *executableProgram=programList[indexOfExecutableProgram];

	// Obtain a process ID
	PID=OperatingSystem_ObtainAnEntryInTheProcessTable();
	if(PID == NOFREEENTRY)
	{
		return NOFREEENTRY;	
	}


	// Obtain the memory requirements of the program
	processSize=OperatingSystem_ObtainProgramSize(&programFile, executableProgram->executableName);	
	if(processSize == PROGRAMDOESNOTEXIST)
	{
		return PROGRAMDOESNOTEXIST;
	}

	if( processSize == TOOBIGPROCESS)
	{
		return TOOBIGPROCESS;
	}

	// Obtain the priority for the process
	priority=OperatingSystem_ObtainPriority(programFile);

	if(priority == PROGRAMNOTVALID || processSize == PROGRAMNOTVALID)
	{
		return PROGRAMNOTVALID;
	}
	
	// Obtain enough memory space
 	loadingPhysicalAddress=OperatingSystem_ObtainMainMemory(processSize, PID);
 	if(loadingPhysicalAddress == TOOBIGPROCESS )
 	{
 		return TOOBIGPROCESS;
 	}

	// Load program in the allocated memory
	int x = OperatingSystem_LoadProgram(programFile, loadingPhysicalAddress, processSize);
	if( x ==  TOOBIGPROCESS)
	{
		return TOOBIGPROCESS;
	}
	
	// PCB initialization
	OperatingSystem_PCBInitialization(PID, loadingPhysicalAddress, processSize, priority, indexOfExecutableProgram);
	
	// Show message "Process [PID] created from program [executableName]\n"
	OperatingSystem_ShowTime(INIT);
	ComputerSystem_DebugMessage(22,INIT,PID,executableProgram->executableName);
	
	return PID;
}


// Main memory is assigned in chunks. All chunks are the same size. A process
// always obtains the chunk whose position in memory is equal to the processor identifier
int OperatingSystem_ObtainMainMemory(int processSize, int PID) {

 	if (processSize>MAINMEMORYSECTIONSIZE)
		return TOOBIGPROCESS;
	
 	return PID*MAINMEMORYSECTIONSIZE;
}


// Assign initial values to all fields inside the PCB
void OperatingSystem_PCBInitialization(int PID, int initialPhysicalAddress, int processSize, int priority, int processPLIndex) {

	processTable[PID].busy=1;
	processTable[PID].initialPhysicalAddress=initialPhysicalAddress;
	processTable[PID].processSize=processSize;
	processTable[PID].state=NEW;
	processTable[PID].priority=priority;
	processTable[PID].programListIndex=processPLIndex;
	// Daemons run in protected mode and MMU use real address
	if (programList[processPLIndex]->type == DAEMONPROGRAM) {
		processTable[PID].copyOfPCRegister=initialPhysicalAddress;
		processTable[PID].queueID = DAEMONSQUEUE;
		processTable[PID].copyOfPSWRegister= ((unsigned int) 1) << EXECUTION_MODE_BIT;
	} 
	else {
		processTable[PID].copyOfPCRegister=0;
		processTable[PID].copyOfPSWRegister=0;
		processTable[PID].queueID = USERPROCESSQUEUE;
	
	}
	
	OperatingSystem_ShowTime(SYSPROC);
	ComputerSystem_DebugMessage(111,SYSPROC, PID,"NEW" );

}


// Move a process to the READY state: it will be inserted, depending on its priority, in
// a queue of identifiers of READY processes
void OperatingSystem_MoveToTheREADYState(int PID) {
	
	if (Heap_add(PID, readyToRunQueue[processTable[PID].queueID],QUEUE_PRIORITY ,&numberOfReadyToRunProcesses[processTable[PID].queueID] ,PROCESSTABLEMAXSIZE)>=0) {	
		OperatingSystem_ShowTime(SYSPROC);
		ComputerSystem_DebugMessage(110,SYSPROC,PID, statesNames[processTable[PID].state], "READY");
		processTable[PID].state=READY;
	} 
	//OperatingSystem_PrintReadyToRunQueue();
}


// The STS is responsible of deciding which process to execute when specific events occur.
// It uses processes priorities to make the decission. Given that the READY queue is ordered
// depending on processes priority, the STS just selects the process in front of the READY queue
int OperatingSystem_ShortTermScheduler() {
	
	int selectedProcess;

	selectedProcess=OperatingSystem_ExtractFromReadyToRun();

	return selectedProcess;
} 


// Return PID of more priority process in the READY queue
int OperatingSystem_ExtractFromReadyToRun() {
  
	int selectedProcess=NOPROCESS;

	if(numberOfReadyToRunProcesses[USERPROCESSQUEUE] > 0 )
		selectedProcess=Heap_poll(readyToRunQueue[USERPROCESSQUEUE],QUEUE_PRIORITY ,&numberOfReadyToRunProcesses[USERPROCESSQUEUE]);
	else
		selectedProcess=Heap_poll(readyToRunQueue[DAEMONSQUEUE],QUEUE_PRIORITY ,&numberOfReadyToRunProcesses[DAEMONSQUEUE]);
	return selectedProcess;

}


// Function that assigns the processor to a process
void OperatingSystem_Dispatch(int PID) {

	// The process identified by PID becomes the current executing process
	executingProcessID=PID;
	// Change the process' state
	
	OperatingSystem_ShowTime(SYSPROC);
	ComputerSystem_DebugMessage(110,SYSPROC, PID, statesNames[processTable[PID].state], "EXECUTING");
	processTable[PID].state=EXECUTING;
	// Modify hardware registers with appropriate values for the process identified by PID
	OperatingSystem_RestoreContext(PID);
}


// Modify hardware registers with appropriate values for the process identified by PID
void OperatingSystem_RestoreContext(int PID) {
  
	// New values for the CPU registers are obtained from the PCB
	Processor_CopyInSystemStack(MAINMEMORYSIZE-1,processTable[PID].copyOfPCRegister);
	Processor_CopyInSystemStack(MAINMEMORYSIZE-2,processTable[PID].copyOfPSWRegister);
	Processor_SetAccumulator(processTable[PID].accumulator);
	// Same thing for the MMU registers
	MMU_SetBase(processTable[PID].initialPhysicalAddress);
	MMU_SetLimit(processTable[PID].processSize);
}


// Function invoked when the executing process leaves the CPU 
void OperatingSystem_PreemptRunningProcess() {

	// Save in the process' PCB essential values stored in hardware registers and the system stack
	OperatingSystem_SaveContext(executingProcessID);
	// Change the process' state
	OperatingSystem_MoveToTheREADYState(executingProcessID);
	// The processor is not assigned until the OS selects another process
	executingProcessID=NOPROCESS;
}


// Save in the process' PCB essential values stored in hardware registers and the system stack
void OperatingSystem_SaveContext(int PID) {
	
	// Load PC saved for interrupt manager
	processTable[PID].copyOfPCRegister=Processor_CopyFromSystemStack(MAINMEMORYSIZE-1);
	// Load PSW saved for interrupt manager
	processTable[PID].copyOfPSWRegister=Processor_CopyFromSystemStack(MAINMEMORYSIZE-2);
	processTable[PID].accumulator = Processor_GetAccumulator();
	
}


// Exception management routine
void OperatingSystem_HandleException() {
  
	// Show message "Process [executingProcessID] has generated an exception and is terminating\n"
	OperatingSystem_ShowTime(SYSPROC);
	ComputerSystem_DebugMessage(23,SYSPROC,executingProcessID);
	
	OperatingSystem_TerminateProcess();
}


// All tasks regarding the removal of the process
void OperatingSystem_TerminateProcess() {
	
	int selectedProcess;
	
	OperatingSystem_ShowTime(SYSPROC);
  	ComputerSystem_DebugMessage(110,SYSPROC, executingProcessID, statesNames[processTable[executingProcessID].state], "EXIT" );
	processTable[executingProcessID].state=EXIT;
	
	// One more process that has terminated
	numberOfNotTerminatedUserProcesses--;
	
	
	if (numberOfNotTerminatedUserProcesses<=0 ) {
		// Simulation must finish 
		OperatingSystem_ReadyToShutdown();
		
		// I dont know why it dont change the executingProcessID
		// so i changed manually to simulate the norma lend
	}
	
	// Select the next process to execute (sipID if no more user processes)
	selectedProcess=OperatingSystem_ShortTermScheduler();
	// Assign the processor to that process
	OperatingSystem_Dispatch(selectedProcess);

}

// System call management routine
void OperatingSystem_HandleSystemCall() {
  
	int systemCallID;
	int pid;
	int temPID;
	
	// Register A contains the identifier of the issued system call
	systemCallID=Processor_GetRegisterA();
	
	switch (systemCallID) {
		case SYSCALL_PRINTEXECPID:
			// Show message: "Process [executingProcessID] has the processor assigned\n"
			ComputerSystem_DebugMessage(24,SYSPROC,executingProcessID);
			break;

		case SYSCALL_END:
			// Show message: "Process [executingProcessID] has requested to terminate\n"
			OperatingSystem_ShowTime(SYSPROC);
			ComputerSystem_DebugMessage(25,SYSPROC,executingProcessID);
			OperatingSystem_TerminateProcess();
			OperatingSystem_PrintStatus();

			break;
		case SYSCALL_YIELD:
	
		
			if(numberOfReadyToRunProcesses[processTable[executingProcessID].queueID]>0)
			{
				pid = readyToRunQueue[processTable[executingProcessID].queueID][0];
				PCB pcbObject = processTable[pid];
				if(pcbObject.priority == processTable[executingProcessID].priority )
				{
					temPID = executingProcessID;
					
					pid = OperatingSystem_ShortTermScheduler();
					OperatingSystem_PreemptRunningProcess();
					OperatingSystem_Dispatch(pid);

					OperatingSystem_ShowTime(SHORTTERMSCHEDULE);
					ComputerSystem_DebugMessage(115, SHORTTERMSCHEDULE,temPID, pid);
					OperatingSystem_PrintStatus();

				}
			}
			break;
		case SYSCALL_SLEEP:
			OperatingSystem_ShowTime(SYSPROC);
			ComputerSystem_DebugMessage(110,SYSPROC,executingProcessID, statesNames[processTable[executingProcessID].state], "BLOCKED");
			processTable[executingProcessID].state = BLOCKED;

			int timeSleeping=0;
			if(Processor_GetAccumulator()>0)
				timeSleeping+=Processor_GetAccumulator();
			else
				timeSleeping+= (-Processor_GetAccumulator());
			
			timeSleeping+= 1 +numberOfClockInterrupts;
			processTable[executingProcessID].whenToWakeUp= timeSleeping;
			
			Heap_add( executingProcessID, sleepingProcessesQueue ,QUEUE_WAKEUP, &numberOfSleepingProcesses,PROCESSTABLEMAXSIZE);
			OperatingSystem_SaveContext(executingProcessID);
			OperatingSystem_PrintStatus(); 

			int pid = OperatingSystem_ShortTermScheduler();
			OperatingSystem_Dispatch(pid);

			break;
	}
}
	
//	Implement interrupt logic calling appropriate interrupt handle
void OperatingSystem_InterruptLogic(int entryPoint){
	switch (entryPoint){
		case SYSCALL_BIT: // SYSCALL_BIT=2
			OperatingSystem_HandleSystemCall();
			break;
		case EXCEPTION_BIT: // EXCEPTION_BIT=6
			OperatingSystem_HandleException();
			OperatingSystem_PrintStatus();
			break;
		case CLOCKINT_BIT:
			OperatingSystem_HandleClockInterrupt(); //CLOCKINT_BIT = 9
			
	}

}

void OperatingSystem_PrintReadyToRunQueue()
{
	int i;
	OperatingSystem_ShowTime(SHORTTERMSCHEDULE);
	ComputerSystem_DebugMessage(106, SHORTTERMSCHEDULE );
	ComputerSystem_DebugMessage(118,SHORTTERMSCHEDULE );
	ComputerSystem_DebugMessage(112, SHORTTERMSCHEDULE );
	for(i = 0; i < numberOfReadyToRunProcesses[USERPROCESSQUEUE];i++)
	{
		int pid = readyToRunQueue[USERPROCESSQUEUE][i];
		
		PCB pcbObject = processTable[pid];
		
		ComputerSystem_DebugMessage(107, SHORTTERMSCHEDULE, pid, pcbObject.priority );
		
	}
	
	ComputerSystem_DebugMessage(108, SHORTTERMSCHEDULE );
	ComputerSystem_DebugMessage(118,SHORTTERMSCHEDULE );
	ComputerSystem_DebugMessage(113, SHORTTERMSCHEDULE );

	for(i = 0; i < numberOfReadyToRunProcesses[DAEMONSQUEUE];i++)
	{
		int pid = readyToRunQueue[DAEMONSQUEUE][i];
		
		PCB pcbObject = processTable[pid];
		
		ComputerSystem_DebugMessage(107, SHORTTERMSCHEDULE, pid, pcbObject.priority );
		
	}

	 
	ComputerSystem_DebugMessage(108, SHORTTERMSCHEDULE );
}

